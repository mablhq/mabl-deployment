# Versioning

This repo uses [semversioner](https://bitbucket.org/bitbucketpipelines/semversioner/src/master/) to maintain a changelist and update versions.

See the semversioner README for installation instructions and usage examples.

# Change log

With each commit, please use semversioner to add the appropriate json file to the `.changes` directory.

```
semversioner add-change --type patch --description 'Initial release'
```

The type value can be: major, minor, patch

If you bump the semverversion, you will also need to update the `bitbucket-pipeline.yml` file to point to the new version for testing.

# Build pipeline

The build pipeline includes:

1.  Bumping the current version using semversioner.
2.  Generating `CHANGELOG.md`
3.  Updating version numbers in the README.md and pipe.yml file.
4.  Pushing docker image.
5.  Tagging the version and pushing changes.

# Publish pipeline

1. Update the [mabl Pipe version](https://bitbucket.org/bitbucketpipelines/official-pipes/src/master/pipes/mabl-deployment.yml) in the Marketplace repo (fork)
2. Submit fork as PR back to BitBucket

# Tests

To run the tests locally, you will need an API key and a mabl application ID that work. The mabl tests kicked off should succeed.

It helps to have a mabl application with no tests, so these tests complete quickly.

The tests are written using [bats](https://github.com/bats-core/bats-core/) so you will have to install that locally to run tests. You should be able to use bats 0.4.0, which is the version installed on ubuntu by the [bats package](https://packages.ubuntu.com/search?keywords=bats)

```
# Run in the repo root directory.
MABL_API_KEY=key MABL_APPLICATION_ID=appid bats test/test.bats
```

# Dockerfile linting

The Dockerfile is linted as part of the build. You can run the linter locally:

```
docker run --rm -i hadolint/hadolint:v1.17.3-3-gae8601e < Dockerfile
```
