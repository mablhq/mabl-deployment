FROM mablhq/mabl-cli:1.11.3

COPY pipe/ ./pipe/

ENTRYPOINT ["/home/mabl/pipe/pipe.sh"]
