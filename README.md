# Bitbucket Pipelines Pipe: mabl deployment

Create a deployment event in [mabl](https://www.mabl.com/) to run end-to-end tests against your application and take action based on the test results.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: mablhq/mabl-deployment:1.1.0
  variables:
    MABL_API_KEY: "<string>"
    # At least one of MABL_APPLICATION_ID or MABL_ENVIRONMENT_ID id must be specified.
    # MABL_APPLICATION_ID: "<string>"
    # MABL_ENVIRONMENT_ID: "<string>"
    # MABL_AWAIT_COMPLETION: "<boolean>" # Optional.
    # MABL_FAST_FAILURE: "<boolean>" # Optional.
    # MABL_BROWSERS: "<string>" # Optional.
    # MABL_LABELS: "<string>" # Optional.
    # MABL_URL: "<string>" # Optional.
    # MABL_REBASELINE_IMAGES: "<boolean>" # Optional.
    # MABL_SET_STATIC_BASELINE: "<boolean>" # Optional.
    # MABL_REVISION: "<string>" # Optional.
    # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable                   | Usage                                                                                                                                                                                              |
| -------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| MABL_API_KEY (\*)          | Your mabl api key [available here](https://app.mabl.com/workspaces/-/settings/apis). Should be stored as a secured variable.                                                                       |
| MABL_APPLICATION_ID (\*\*) | mabl id for the deployed application. Use the [mabl CLI example](https://app.mabl.com/workspaces/-/settings/apis) to get the id.                               |
| MABL_ENVIRONMENT_ID (\*\*) | mabl id for the deployed environment. Use the [mabl CLI example](https://app.mabl.com/workspaces/-/settings/apis) to get the id.                                    |
| MABL_AWAIT_COMPLETION      | Wait for the deployment tests to finish before proceeding. Default: `true`                                                                                                                         |
| MABL_FAST_FAILURE          | Stop waiting if one test fails. This allows you to fail your build as soon as possible. Code Insights reports are not generated in fast failure mode. Default: `true`                                                                                            |
| MABL_BROWSERS              | Space separated list of browsers to test against (internet_explorer safari chrome firefox). If not provided, mabl will test the browsers configured on the triggered test.                         |
| MABL_LABELS                | Space separated list of test labels to test against.                                                                                                                                               |
| MABL_URL                   | The base uri to test against. If provided, this will override the default uri associated with the environment in mabl.                                                                             |
| MABL_REBASELINE_IMAGES     | Set `true` to reset the visual baseline to the current deployment. Default: `false`                                                                                                                |
| MABL_SET_STATIC_BASELINE   | Set true to use current deployment as an exact static baseline. If set, mabl will not model dynamic areas and will use the current deployment as the pixel-exact visual baseline. Default: `false` |
| MABL_REVISION              | The code revision hash for the application under test. Defaults to the current commit hash.                                                                                                        |
| DEBUG                      | Enable debug logging. By default debug logging is disabled.                                                                                                                                        |

_(\*) = required variable._

_(\*\*) = MABL_APPLICATION_ID or MABL_ENVIRONMENT_ID must be specified, or both._

## Details

This [Bitbucket Pipe](https://bitbucket.org/product/features/pipelines/integrations) allows you to easily integrate your Bitbucket pipeline with [mabl](https://www.mabl.com/) and test the UI of your application. When the pipe is triggered, it will create a deployment event in mabl which in turn will trigger test runs for the deployment. The pipe will wait for completion of the tests and will return success if all tests have passed or fail if any tests fail.

## Prerequisites

We recommend storing your mabl API key as a secured variable `MABL_API_KEY` in your Bitbucket repository as shown on the example below. You can define such variables from the Bitbucket settings menu on an account, repository and deployment environment levels. See the [Bitucket documentation](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html) for details.

## Examples

Basic examples:

Run all tests across environments against the specified application.

```yaml
- pipe: mablhq/mabl-deployment:1.1.0
  variables:
    MABL_API_KEY: $MABL_API_KEY
    MABL_APPLICATION_ID: "application123-a"
```

Run all tests against all apps with the specified environment.

```yaml
- pipe: mablhq/mabl-deployment:1.1.0
  variables:
    MABL_API_KEY: $MABL_API_KEY
    MABL_ENVIRONMENT_ID: "environment123-e"
```

Run all tests in plans with a given label against the specified application and environment.

```yaml
- pipe: mablhq/mabl-deployment:1.1.0
  variables:
    MABL_API_KEY: $MABL_API_KEY
    MABL_APPLICATION_ID: "application123-a"
    MABL_ENVIRONMENT_ID: "environment123-e"
    MABL_LABELS: "regression"
```

Advanced example:
Run all tests in plans with labels `smoketest` and `regression` in the four major browsers (Chrome, Firefox, Safari, Internet Explorer), starting from the specified URL, and use these test runs to set a static baseline for visual change detection. mabl will also wait for any configured plan retries to complete before deciding whether the test results should be considered passed or failed. The code revision hash can be used to filter test results in mabl and serve as a reference between the results and your code changes.

```yaml
- pipe: mablhq/mabl-deployment:1.1.0
  variables:
    MABL_API_KEY: $MABL_API_KEY
    MABL_APPLICATION_ID: "application123-a"
    MABL_ENVIRONMENT_ID: "environment-e"
    MABL_AWAIT_COMPLETION: "true"
    MABL_FAST_FAILURE: "false"
    MABL_BROWSERS: "internet_explorer chrome firefox safari"
    MABL_LABELS: "smoketest regression"
    MABL_URL: "https://staging.mycompany/"
    MABL_SET_STATIC_BASELINE: "true"
    MABL_REVISION: "600d8eb"
```

## Support

If you would like help with this pipe, or would like to report an issue, please [contact mabl support](https://help.mabl.com/page/mabl-support-overview).
