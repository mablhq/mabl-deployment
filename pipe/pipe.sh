#!/bin/bash
set -e

source "$(dirname "$0")/common.sh"

debug "mabl cli version: $(mabl --version)"

# Required parameters
MABL_API_KEY=${MABL_API_KEY:?'The required MABL_API_KEY variable is not set.'}

# Default values
MABL_AWAIT_COMPLETION=${MABL_AWAIT_COMPLETION:-true}
MABL_FAST_FAILURE=${MABL_FAST_FAILURE:-true}

# At least one of Application ID or Environment Id is required
if [[ -z "${MABL_APPLICATION_ID}" ]] && [[ -z "${MABL_ENVIRONMENT_ID}" ]]; then
    fail "At least one of MABL_APPLICATION_ID or MABL_ENVIRONMENT_ID variables is required."
fi

# Build up optional parameters
deployment_opts=()

if [[ -n "${MABL_APPLICATION_ID}" ]]; then
    deployment_opts+=("--application-id \"${MABL_APPLICATION_ID}\"")
fi

if [[ -n "${MABL_ENVIRONMENT_ID}" ]]; then
    deployment_opts+=("--environment-id \"${MABL_ENVIRONMENT_ID}\"")
fi

if [[ "${MABL_AWAIT_COMPLETION,,}" == "true" ]]; then
    deployment_opts+=("--await-completion")
fi

if [[ "${MABL_FAST_FAILURE,,}" == "true" ]]; then
    deployment_opts+=("--fast-failure")
fi

if [[ -n "${MABL_BROWSERS}" ]]; then
    deployment_opts+=("--browsers \"${MABL_BROWSERS}\"")
fi

if [[ -n "${MABL_LABELS}" ]]; then
    deployment_opts+=("--labels \"${MABL_LABELS}\"")
fi

if [[ -n "${MABL_URL}" ]]; then
    deployment_opts+=("--url \"${MABL_URL}\"")
fi

if [[ "${MABL_REBASELINE_IMAGES,,}" == "true" ]]; then
    deployment_opts+=("--rebaseline-images true")
fi

if [[ "${MABL_SET_STATIC_BASELINE,,}" == "true" ]]; then
    deployment_opts+=("--set-static-baseline true")
fi

# Bitbucket build context
if [[ -n "${BITBUCKET_COMMIT}" ]]; then
    deployment_opts+=("--revision \"${BITBUCKET_COMMIT}\"")
fi

# auth step is required for describe command
mabl auth activate-key "${MABL_API_KEY}"

# set custom pipe flag on for proper Reports API proxy settings
export IS_CUSTOM_BITBUCKET_PIPE="true"

# Create the deployment
debug "Deployment options: ${deployment_opts[@]})"
FORCE_COLOR=1 mabl deployments create --await-completion ${deployment_opts[@]}
