#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/mabl-deployment"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Creating deployment event should succeed" {
    run docker run \
        -e MABL_API_KEY="$MABL_API_KEY" \
        -e MABL_APPLICATION_ID="$MABL_APPLICATION_ID" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Creating deployment event with boolean values should succeed" {
    run docker run \
        -e MABL_API_KEY="$MABL_API_KEY" \
        -e MABL_APPLICATION_ID="$MABL_APPLICATION_ID" \
        -e MABL_REBASELINE_IMAGES=true \
        -e MABL_SET_STATIC_BASELINE=true \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Creating deployment event with bad api key should fail" {
    skip "Re-add this test when CLI fixes this issue."
    run docker run \
        -e MABL_API_KEY="bad-credentials" \
        -e MABL_APPLICATION_ID="$MABL_APPLICATION_ID" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -ne 0 ]
}

@test "Creating deployment event with no api key should fail" {
    run docker run \
        -e MABL_APPLICATION_ID="$MABL_APPLICATION_ID" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -ne 0 ]
}
