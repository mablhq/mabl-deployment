#!/usr/bin/env bash
set -eu
# Release to dockerhub.
#
# On master, we push both a :latest tag, and :<next-version>
# On release/<foo> branches, we only push :<next-version>
#
# Required globals:
#   DOCKERHUB_USERNAME
#   DOCKERHUB_PASSWORD
#   BITBUCKET_BRANCH
#   BITBUCKET_BUILD_NUMBER
#
# Arguments:
#   REPOSITORY: The dockerhub repository we are pushing to.
#
set -ex

REPOSITORY=$1
VERSION=$(semversioner current-version)

# Login to dockerhub.
docker_login() {
    echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
}

# Generate all tags.
generate_tags() {
    if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
        tags=("${VERSION}")
    elif [[ "${BITBUCKET_BRANCH}" =~ "release-" ]]; then
        tags=("${VERSION}")
    elif [[ "${BITBUCKET_BRANCH}" =~ "qa-" ]]; then
        tags=("${VERSION}-qa-${BITBUCKET_BUILD_NUMBER}")
    else
        echo "ERROR: Can only push docker images from master, qa-* or release-* branches."
        exit 1
    fi
}

# Build and tag with an intermediate tag.
docker_build() {
    docker build -t ${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER} .
}

# Tag with final tags and push.
docker_push() {

    # Note: you've got to login to get the list of tags
    echo "Logging into DockerHub API"
    DOCKERHUB_TOKEN=$(curl \
      -s -H "Content-Type: application/json" \
      -X POST \
      -d '{"username": "'${DOCKERHUB_USERNAME}'", "password": "'${DOCKERHUB_PASSWORD}'"}' \
      https://hub.docker.com/v2/users/login/ | jq -r .token)

    for tag in "${tags[@]}"; do

        # Check if the tag exists
        echo "Checking if tag [${tag}] already published"
        set +e
        TAG_MATCH_COUNT=$(curl -s \
            -H "Authorization: JWT ${TOKEN}" \
            "https://hub.docker.com/v2/repositories/${REPOSITORY}/tags/?page_size=10000" | jq .results[].name | grep -c "\"${tag}\"")
        set -e

        # Don't push the image,if we already have the image published - it needs a new tag!
        if [[ "${TAG_MATCH_COUNT}" != "0" ]]; then
          echo "WARNING: DockerHub ALREADY has the tag [${tag}]. Skipping publishing."
          continue
        fi

        docker tag "${REPOSITORY}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${REPOSITORY}:${tag}"
        docker push "${REPOSITORY}:${tag}"
    done
}

docker_login
generate_tags
docker_build
docker_push
