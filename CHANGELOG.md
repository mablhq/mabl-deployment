# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: upgrade mabl cli to version 1.10.9 to pick up improvements to exit codes on unexpected failures

## 1.0.2

- patch: Update image reference

## 1.0.1

- patch: Update pipe.yml format

## 1.0.0

- major: GA mabl CLI Release 1.6.17

## 0.5.1

- patch: Fix unreleased path bug

## 0.5.0

- minor: mabl-cli 0.6.81 release

## 0.4.3

- patch: Update readme info for getting application and environment ids

## 0.4.2

- patch: enable pipe reports API auth

## 0.4.1

- patch: turn Code Insights integration on by default

## 0.4.0

- minor: bump the mabl-cli dependency for Code Insights integration

## 0.3.2

- patch: Fix pipe name in readme.

## 0.3.1

- patch: Fix pipe.yml

## 0.3.0

- minor: Update to readme and logo.

## 0.2.0

- minor: Update to mabl cli 0.2.5-beta.

## 0.1.1

- patch: Remove jq dependency.

## 0.1.0

- minor: Initial version
